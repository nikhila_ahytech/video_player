<html>
<head>
    <title>test Player</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/ahy.css">
  <link rel="stylesheet" href="../css/maier.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/creative.js"></script>
  <style type="text/css">
   /*Content video type ends*/
/*Player Starts*/
.player{
        margin: 5px 0;
    padding: 0;
    position: absolute;
    bottom: 0;
    width: 100%;
    left: 0;
    height: 40px;
    line-height: 40px;
    /*display: none;*/
    font-size: 30px;
}
.IMRT-player{
    padding:0;
    /*cursor: pointer;*/
}

.IMRT-fullscreen{margin: 10px 0;}
.IMRT-player-controls{
    text-align: center;
    color: #65caea;
    /*font-size: 30px;*/
    padding:0;
    margin: 4px 0;
}
.IMRT-play-pause{
    cursor:pointer;
}

.IMRT-progressbar-wrapper{cursor: pointer;display: block;padding: 0;}
.volume-bar-wrapper{cursor: pointer;height: 100%;}
.IMRT-progressbar{
    /*height: 10px;*/
    height:5px;
    background: #2b8c9e;
    position: relative;
    /*top: 17px;*/
    padding:0;
    cursor: pointer;
    margin: 17px 0;
}
.IMRT-tooltiptext{
   /*display: none;*/
   background-color: #fff;
   color: #000;
   font-weight: 900;
   text-align: center;
   border-radius: 6px;
   padding: 5px 2px;
   position: absolute;
   z-index: 1;
   bottom: 170%;
   left: 0;
}
.IMRT-loaded{
    width: 0%;
    /*height: 10px;*/
    /*height: 20px;*/
    background: #25626d;
    cursor: pointer;
    /*top: 15px;*/
    transition: all 0.1s ease;
    -webkit-transition: all 0.1s ease;
    -moz-transition:all 0.1s ease;
    -o-transition:all 0.1s ease;
}
.IMRT-progress{
    position: absolute;
    width: 0%;
    height: 100%;
    top: 0;
    left: 0;
    background: #2de8e4;
    transition: all 0.1s ease;
    -webkit-transition: all 0.1s ease;
    -moz-transition:all 0.1s ease;
    -o-transition:all 0.1s ease;
}
.IMRT-volume-controll{
    /*font-size: 30px;*/
    color: #65caea;
}
.IMRT-volume-bar{
    position: relative;
    /*width: 60%;*/
    width: 65%;
    background: #2b8c9e;
    /*height: 7px;*/
    height: 5px;
    float: right;
    display: inline-block;
    /*bottom: 8px;*/
    cursor : pointer;
    margin: 17px 0;
}
.IMRT-volume-seeker{
    position: absolute;
    width: 0%;
    height: 100%;
    background: #2de8e4;
    top:0;
    left: 0;
    transition: all 0.1s ease;
    -webkit-transition: all 0.1s ease;
    -moz-transition:all 0.1s ease;
    -o-transition:all 0.1s ease;
}
.IMRT-volume-controll{
    cursor:pointer;
    display: inline-block;
    /*width: 30%;*/
    float: left;
    text-align: center;
    margin: 5px 0;
    
}
.IMRT-time,.IMRT-fullscreen{
    /*padding: 5px 10px;*/
    text-align: center;
    font-size: 20px;
    color: #65caea;
    font-weight: 900;
}
.IMRT-player-overlay{
    /*display: none !important;*/
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    background: transparent;
    color: #fff;
    cursor: pointer;
}
.IMRT-error-overlay{
    display: none;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    z-index: 2;
    background: black;
    color:#fff;
    font-size: 1em;
}
.IMRT-overlay{
    width: 100%;
    height: 100%;
    text-align: center;
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
}
.IMRT-error-overlay:before,.IMRT-player-overlay:before{
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -0.25em;
}
.IMRT-overlay-play{
    font-size: 100px;
    color: red;
    display: inline-block;
    vertical-align: middle;
}
.IMRT-error{
    width:100%;
    height:100%;
}
.IMRT-error:before{
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
}
.IMRT-exit{
    display: none;
}
.IMRT-exit,.IMRT-full{
    cursor:pointer;
}
.video-embed iframe{
    width:100%;
    height: 100%;
}
.player-fullwidth{
    width: 100%;
}

@media screen and (max-width: 768px){
    .IMRT-time{font-size: 10px;}
    .player{font-size: 20px;}
    .player{margin: 0;}
}

@media screen and (max-width: 374px){
    .player {height: 20px !important;line-height: 20px !important;font-size: 13px!important;}
    .IMRT-progressbar{margin: 7px 0 !important;}
    .IMRT-player-controls,.IMRT-volume-controll{margin: 3px 0px !important;}
    .IMRT-time{font-size: 9px !important;margin: 0 !important;}
    .IMRT-fullscreen{margin: 2px 0 !important;font-size: 15px !important;}

}

@media screen and (max-width: 768px) and (orientation:portrait){ 
    .IMRT-volume-bar{display: none;}
    .player{height: 25px;line-height: 25px;}
    .IMRT-player-controls, .IMRT-volume-controll{margin: 2px 0px;}
    .IMRT-time{padding:0;}
    .IMRT-progressbar{margin: 9px 3px;}
    .IMRT-fullscreen {margin: 3px 0;}
    .IMRT-time{font-size: 12px;}

}

@media screen and (max-width: 768px) and (orientation:landscape){ 
   .player {margin: 0;height: 35px;line-height: 35px;}
   .IMRT-player-controls,.IMRT-volume-controll,.IMRT-fullscreen{margin: 7px 0;}
   .IMRT-progressbar{margin: 14px 0;}
   .IMRT-time{font-size: 13px;}
   .IMRT-volume-controller{padding: 0;margin: 0 10px;}
   .IMRT-volume-bar{width: 70%;margin: 14px 0;}
}
@media screen and (max-width:991px) and (min-width: 769px){ 
    .player{line-height: 36px;height: 36px;margin: 0;font-size: 22px;}
    .IMRT-time{font-size: 15px;padding: 0;}
    .IMRT-fullscreen {margin: 8px 0;}
    .IMRT-volume-bar{width: 70%;margin: 15px 0;}
    .IMRT-volume-controll,.IMRT-player-controls{margin: 6px 0;}
    .IMRT-progressbar{margin: 15px 0;}
}



/*Player Ends*/
  </style>
</head>
<body>

    <div class="modal-contents col-md-offset-2 col-lg-offset-2 col-sm-offset-2 col-md-8 col-lg-8 col-sm-8 col-xs-12">
        <span type="button" class="close ahy-close" data-dismiss="modal">X</span>
        <div class="content-custom-video embed-responsive-16by9 embed-responsive player-fullwidth" id="embed-imrt-746">
            <div class="video-embed">
                <video id="imrt-746" preload="metadata">
                <source src="http://imreallythere.staging.wpengine.com/wp-content/uploads/2016/08/Teri-Khair-Mangdi-Baar-Baar-Dekho-Sidharth-Malhotra-Katrina-Kaif-Bilal-Saeed.mp4" type="video/mp4">
                Your browser does not support the video tag.
                </video>
            </div>
            <div class="player col-xs-12 col-sm-12 col-md-12 col-lg-12" type="custom-video">
                <div class="IMRT-player col-xs-12 col-sm-12 col-md-12 col-lg-12" player-id="imrt-746">
                    <div class="IMRT-player-controls col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <span class="IMRT-play-pause" status="play">
                            <i class="fa fa-play" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-6 col-lg-6 IMRT-progressbar-wrapper">
                        <div class="IMRT-progressbar">
                            <span class="IMRT-tooltiptext">0:50</span>
                            <div class="IMRT-loaded">
                                <div class="IMRT-progress"></div>
                            </div>
                        </div>
                    </div>
                    <div class="IMRT-volume-controller col-xs-2 col-sm-3 col-md-2 col-lg-2">
                        <span class="IMRT-volume-controll">
                            <i class="fa fa-volume-up" aria-hidden="true"></i>
                        </span>
                        <div class="volume-bar-wrapper">
                            <div class="IMRT-volume-bar">
                                <div class="IMRT-volume-seeker" style="width: 33%;">
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="IMRT-time col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="IMRT-current-time">0:00</span>/<span class="IMRT-total-time">2:56</span>
                    </div>
                    <div class="IMRT-fullscreen col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <span class="IMRT-full">
                            <i class="fa fa-expand" aria-hidden="true"></i>
                        </span>
                        <span class="IMRT-exit">
                            <i class="fa fa-compress" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="IMRT-player-overlay text-center" style="display: none; background-image: url(&quot;http://imreallythere.staging.wpengine.com/wp-content/uploads/2016/08/tony9n-3-web.jpg&quot;);" player-id="imrt-746">
                <span class="IMRT-overlay-play">
                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                </span>
            </div>
            <div class="IMRT-error-overlay text-center" player-id="imrt-746">
                <span class="IMRT-player-error">Something went wrong!</span>
                <span class="IMRT-player-reload"><a href="http://imreallythere.staging.wpengine.com/?post_type=post&amp;p=746">Reload</a></span>
        
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 post-footer">
            <div class="post-footer-layer-1 col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                <div class="post-title col-xs-12 col-sm-8 col-md-9 col-lg-9">
                    <h3 class="modal-title text-center">Uploaded  video</h3>
                    <span class="subtitle">This post contains .mp4 video</span>
                </div>
                <div class="comment-div-btn col-xs-12 col-sm-4 col-md-3 col-lg-3"><button type="button" class="btn btn-default  IMRT-comments-btn cat-3" post-id="746" style="background:#1bc134">Post Comment</button><p class="ahy-total-comments-746 ahy-total-comment">0</p>
                </div>
            </div>
            <hr>
            <div class="post-footer-layer-2 col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="post-details-wrap col-md-8 col-lg-8 col-sm-12 col-xs-12">
                    <div class="post-date">
                        <h3>Published&nbsp;30 August 2016&nbsp;To&nbsp;</h3>
                        <ul class="post-categories">
                            <li>
                                <a href="http://imreallythere.staging.wpengine.com/./music/" rel="category tag" class="cat-3">Music</a>
                            </li>
                        </ul>
                    </div>

                    <div class="post-tags">
                        <a href="http://imreallythere.staging.wpengine.com/tag/custom/" rel="tag">#custom</a>
                    </div>
                </div>

                <div class="post-social col-md-4 col-lg-4 col-sm-12 col-xs-12">      
                    <div class="social-sharing ss-social-sharing">
                        <a onclick="return ss_plugin_loadpopup_js(this);" rel="external nofollow" class="modal_social_icon ss-button-twitter" href="http://twitter.com/intent/tweet/?text=Uploaded++video&amp;url=http%3A%2F%2Fimreallythere.staging.wpengine.com%2Fuploaded-video%2F" target="_blank">                         
                            <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
                        
                        </a>
                        <a onclick="return ss_plugin_loadpopup_js(this);" rel="external nofollow" class="modal_social_icon ss-button-facebook" href="http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fimreallythere.staging.wpengine.com%2Fuploaded-video%2F" target="_blank">                          
                            <span><i class="fa fa-facebook" aria-hidden="true"></i></span>
                            
                        </a>
                        <a onclick="return ss_plugin_loadpopup_js(this);" rel="external nofollow" class="modal_social_icon last-icon ss-button-googleplus" href="https://plus.google.com/share?url=http%3A%2F%2Fimreallythere.staging.wpengine.com%2Fuploaded-video%2F" target="_blank">                            
                            <span><i class="fa fa-google-plus" aria-hidden="true"></i></span>
                             
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
